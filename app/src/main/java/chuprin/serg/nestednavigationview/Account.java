package chuprin.serg.nestednavigationview;

import android.support.annotation.DrawableRes;

public class Account {
    private String mName;
    private String mEmail;
    private @DrawableRes int mAvatar;

    public Account(@DrawableRes int avatar, String name, String email) {
        mAvatar = avatar;
        mName = name;
        mEmail = email;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public int getAvatar() {
        return mAvatar;
    }

    public String getEmail() {
        return mEmail;
    }
}
