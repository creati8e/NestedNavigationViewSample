package chuprin.serg.nestednavigationview;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

public class DataBindingUtils {
    @BindingAdapter("android:src")
    public static void setImageResource(ImageView imageView, int resource) {
        imageView.setImageResource(resource);
    }
}
