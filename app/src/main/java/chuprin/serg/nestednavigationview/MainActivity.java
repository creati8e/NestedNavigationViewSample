package chuprin.serg.nestednavigationview;

import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;

import chuprin.serg.nestednavigationview.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding mBinding;
    private NavigationViewFragment mNavigationFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mNavigationFragment = (NavigationViewFragment) getSupportFragmentManager()
                .findFragmentById(R.id.navigationViewFragment);
        setNavigationViewWidth();
        setupDrawerToggle();
        mBinding.drawerLayout.addDrawerListener(new DrawerListener());
    }


    private void setupDrawerToggle() {
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this,
                mBinding.drawerLayout, mBinding.toolbar,
                R.string.app_name, R.string.app_name);
        mBinding.drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    private void setNavigationViewWidth() {
        if (!isTablet()) {
            return;
        }
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = isLandscape() ? metrics.widthPixels / 2 : metrics.widthPixels - metrics.widthPixels / 3;
        DrawerLayout.LayoutParams params = new DrawerLayout.LayoutParams(width, ViewGroup.LayoutParams.MATCH_PARENT);
        params.gravity = GravityCompat.START;
        mNavigationFragment.getView().setLayoutParams(params);
    }

    private boolean isTablet() {
        return getResources().getBoolean(R.bool.is_tablet);
    }

    private boolean isLandscape() {
        return getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    private class DrawerListener extends DrawerLayout.SimpleDrawerListener {
        @Override
        public void onDrawerSlide(View drawerView, float slideOffset) {
            mBinding.relativeLayout.setTranslationX(drawerView.getWidth() * slideOffset);
        }
    }
}
