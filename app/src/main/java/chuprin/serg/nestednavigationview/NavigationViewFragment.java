package chuprin.serg.nestednavigationview;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SlidingPaneLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import java.util.ArrayList;
import java.util.List;

import chuprin.serg.nestednavigationview.databinding.FragmentNavigationViewBinding;

public class NavigationViewFragment extends Fragment {
    private FragmentNavigationViewBinding mBinding;
    private AccountsAdapter mAccountsAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation_view, container, false);
        mBinding = DataBindingUtil.bind(view);
        mBinding.editFab.hide();
        setupAdapter();
        fillSampleData();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding.slidingPanel.setPanelSlideListener(new SlidePanelListener());
        int color = ContextCompat.getColor(getActivity(), R.color.cardview_light_background);
        mBinding.slidingPanel.setSliderFadeColor(color);
        setPrimaryNavViewWidth(view);
    }

    private void setupAdapter() {
        mAccountsAdapter = new AccountsAdapter(fillSampleData());
        mBinding.primaryNavView.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mBinding.primaryNavView.recyclerView.setAdapter(mAccountsAdapter);
    }

    private List<Account> fillSampleData() {
        ArrayList<Account> accounts = new ArrayList<>();
        accounts.add(new Account(R.drawable.ic_michelle, "Michelle", "michele@email.com"));
        accounts.add(new Account(R.drawable.ic_colombo, "Colombo", "colombo@email.com"));
        accounts.add(new Account(R.drawable.ic_christopher, "Christopher", "christopher@email.com"));
        accounts.add(new Account(R.drawable.ic_greg, "Greg", "greg@email.com"));
        accounts.add(new Account(R.drawable.ic_jane, "Jane", "jane@email.com"));
        accounts.add(new Account(R.drawable.ic_chloe, "Chloe", "chloe@email.com"));
        return accounts;
    }

    private void setPrimaryNavViewWidth(View view) {
        view.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int i, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {
                view.removeOnLayoutChangeListener(this);
                int width = view.getWidth();
                SlidingPaneLayout.LayoutParams layoutParams = new SlidingPaneLayout
                        .LayoutParams(width - width / 5, ViewGroup.LayoutParams.MATCH_PARENT);
                mBinding.primaryNavView.scrimInsetLayout.setLayoutParams(layoutParams);
            }
        });
    }

    private class SlidePanelListener implements SlidingPaneLayout.PanelSlideListener {

        @Override
        public void onPanelSlide(View panel, float slideOffset) {
            mBinding.editFab.setX(panel.getX() - mBinding.editFab.getWidth() / 2);

            if (slideOffset > 0.9) {
                mBinding.editFab.show();
            } else {
                mBinding.editFab.hide();
            }
        }

        @Override
        public void onPanelOpened(View panel) {
            setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
        }

        @Override
        public void onPanelClosed(View panel) {
            setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }

        private void setDrawerLockMode(int mode) {
            ViewParent parent = mBinding.rootLayout.getParent();
            if (parent instanceof DrawerLayout) {
                ((DrawerLayout) parent).setDrawerLockMode(mode);
            } else {
                throw new IllegalStateException("Fragment should be a child of DrawerLayout for proper work");
            }
        }
    }
}
